package com.calci.app;

public class Calci {
    
    public static int add (int a, int b) {
        return a + b;
    }

    public static int sub (int a, int b) {
        return a - b;
    }
}