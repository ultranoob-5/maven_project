package com.calci.app;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestCalci {
    @Test
    void testAdd() {
        assertEquals(5, Calci.add(2, 3));
    }
    @Test
    void testSub() {
        assertEquals(5, Calci.sub(8, 3));
    }
}